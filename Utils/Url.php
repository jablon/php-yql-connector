<?php

/**
 * @author Tomáš Jablonický
 */

namespace YQL\Utils;

class Url extends \Nette\Object
{
    
    /** @var string $url */
    private $url;
    
    /** @var array $query */
    private $query = array();
    
    /**
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }
    
    /**
     * @param string $name
     * @param string $value
     */
    public function __set($name, $value) 
    {
        $this->setQuery($name, $value);
    }
    
    /**
     * @param string $name
     */
    public function &__get($name) 
    {
        $this->getQuery($name);
    }
    
    /**
     * @param string $name
     * @param string $value
     * @return \Myjql\Utils\Url
     */
    public function setQuery($name, $value)
    {
        $this->query[$name] = $value;
        return $this;
    }
    
    /**
     * @param string $name
     * @return string
     */
    public function getQuery($name)
    {
        return isset($this->query[$name])?$this->query[$name]:null;
    }
    
    /**
     * @return string
     */
    public function __toString() 
    {
        $query = '?';
        foreach ($this->query as $name => $value)
            if (key( array_slice($this->query, -1, 1, TRUE ) )!=$name) {
                $query .= $name.'='.urlencode($value).'&';
            } else {
                $query .= $name.'='.urlencode($value);
            }
        return $this->url.$query;
    }
}