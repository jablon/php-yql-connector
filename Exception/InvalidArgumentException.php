<?php

/**
 * @author Tomáš Jablonický
 */

namespace YQL\Exception;

class InvalidArgumentException extends LogicException
{

}