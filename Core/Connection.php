<?php

/** 
 * @author Tomáš Jablonický
 */

namespace YQL\Core;

class Connection extends \Nette\Object
{
    
    const DIAGNOSTICS_ENABLE  = TRUE;

    const DIAGNOSTICS_DISABLE = FALSE;
    
    const ENV_STORE          = 'store://datatables.org/alltableswithkeys';
    
    /** @var string $api */
    private $query_url = 'http://query.yahooapis.com/v1/public/yql';
    
    /** @var YQL\Utils\Url $url */
    private $url;
    
    /** @var YQL\Core\Query $query */
    private $query;
    
    /** @var array $config */
    private $config = array();
    
    /**
     * @param array $config
     */
    public function __construct($config = array())
    {
        $this->url = new \YQL\Utils\Url($this->query_url);
        if (isset($config['diagnostics'])) $this->config['diagnostics'] = $config['diagnostics'];
        if (isset($config['env'])) $this->config['env'] = $config['env'];
    }
    
    /**
     * Zapis dotazu
     * @param string $arg
     * @return Query
     */
    public function query($arg)
    {
        return $this->query = new Query($this, $arg);
    }
    
    /**
     *  Vrati vysledek dotazu
     * @return string json
     */
    public function execute()
    {
        $query = $this->query->getQuery();
        $this->url->setQuery('q', $query)
                ->setQuery('format', 'json');
        foreach ($this->config as $key => $value) 
            $this->url->setQuery ($key, $value); 
        
        $curl = curl_init($this->url->__toString());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }
}