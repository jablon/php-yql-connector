<?php

/**
 * @author Tomáš Jablonický
 */

namespace YQL\Core;

class DataResult extends \Nette\ArrayHash
{
    
    /**
     * @param string $name
     * @return null
     */
    public function __get($name) 
    {
        if (!$this->offsetExists($name))
           return null;
    }
}