<?php

/**
 * @author Tomáš Jablonický
 */

namespace YQL\Core;

class Query extends \Nette\Object
{
 
    /** @var Connection $connection */
    protected $connection;
    
    /** @var string $arg */
    private $arg;
    
    /**
     * @param \YQL\Core\Connection $connection
     * @param string $arg
     */
    public function __construct(Connection $connection, $arg)
    {
        $this->arg        = $arg;
        $this->connection = $connection;
    }
    
    /**
     * Vrati url encode string
     * @return string
     */
    public function getQuery()
    {
        return $this->arg;
    }
    
    /**
     * Vrati vysledek v array nebo DataResult
     * @param boolean $array
     * @return DataResult|array
     * @throws \YQL\Exception\QueryException
     */
    public function fetch($array=false)
    {
        $data =json_decode($this->connection->execute(), true);
        
        if (isset($data['error']))
            throw new \YQL\Exception\QueryException($data['error']['description']);
        
        if ($array===false)
            return DataResult::from($data, true);
        return $data;
    }
}