PHP YQL Connector
=================

Popis
---------

Knihovna pro dotazovani na Yahoo API v jazyce YQL uvedená na adrese http://developer.yahoo.com/yql/

Instalace
---------

K instalaci je potřeba Nette ve verzi 2.0 a vyšší.

1. Naclonujte repozitář do vaší adresářové struktůry

2. Nakonfigurujte instanci pro připojení


Příklady
--------

Konfigurace
-----------

<?php

$config = array(
    'diagnostics' => \YQL\Core\Connection::DIAGNOSTICS_ENABLE,
    'env'         => \YQL\Core\Connection::ENV_STORE \\pro ziskani komunitnich dat
);

$connection = new \YQL\Core\Connection($config)

?>

Poznámka: $config je nepovinný, knihovna zatím nepracuje s APIkey - nebylo to zatím potřeba

Položení dotazu s návratem dat
------------------------------

Data lze získat v array nebo v \YQL\Core\DataResource. Práce s objectem je stejná jako 
práce s \Nette\ArrayHas. Neexistující klíč navrátí NULL.

<?php
    
    $result = $connection->query('show tables')->fetch(\* TRUE - pokud chcete array *\);
    dump($result);
?>

Poznámka: pokud je špatná syntaxe dotazu, dojde k vyhození výjimky \YQL\Exception\QueryException

Budoucnost
----------

1. Do konfigurace příjde také APIkey
2. Cache dotazů a výsledků